+++
title = "Weekly GNU-like Mobile Linux Update (16/2023): KDE Gear 23.04 and Linux App Summit 2023"
draft = false
date = "2023-04-23T21:55:00Z"
[taxonomies]
tags = ["Linux App Summit","GNOME Mobile","KDE Gear","postmarketOS","SailfishOS","Manjaro Plasma Mobile","Unified Push",]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = " (with friendly assistance from plata's awesome script)"
+++

Also: A redesigned Flathub and a PureOS Flatpak repo, a Sailfish Report, and make sure to come to [Augsburg on Saturday](https://linmob.net/see-you-in-augsburg-lit-2023/) if you can :) 
 
<!-- more -->
_Commentary in italics._

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#92 Image Printing](https://thisweek.gnome.org/posts/2023/04/twig-92/)
- TingPing: [How GTK3 themes work in Flatpak](https://blog.tingping.se/2023/04/20/Themes-and-Flatpak.html)

#### Plasma ecosystem	
- Nate Graham: [This week in KDE: Stomping all the bugs!](https://pointieststick.com/2023/04/21/this-week-in-kde-stomping-all-the-bugs/)
- KDE Announcement: [KDE Gear 23.04](https://kde.org/announcements/gear/23.04.0/) _The Plasma Mobile apps are now part of KDE Gear, btw._
- Carl Schwan: [Arianna 1.0.1](https://carlschwan.eu/2023/04/22/arianna-1.0.1/)
- rabbitic translator: [Porting to Qt6 in practice](https://rabbitictranslator.com/port-to-qt6/)

#### SailfishOS
- [Sailfish Community News, 20th April, Sandboxing](https://forum.sailfishos.org/t/sailfish-community-news-20th-april-sandboxing/15431)

#### Ubuntu Touch

#### Distributions
- Manjaro PinePhone Plasma Mobile: [Beta 15 RC1](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/beta15-rc1)
- Manjaro PinePhone Plasma Mobile: [Release 202304200351](https://github.com/manjaro-pinephone/plasma-mobile/releases/tag/202304200351)

#### Linux
- CNX Software: [Linux 6.3 release - Notable changes, Arm, RISC-V and MIPS architectures](https://www.cnx-software.com/2023/04/24/linux-6-3-release-notable-changes-arm-risc-v-and-mips-architectures/)

#### Non-Linux
* Lup Yuen: [NuttX RTOS for PinePhone: 4G LTE Modem](https://lupyuen.github.io/articles/lte)

#### Matrix
- Matrix.org: [This Week in Matrix 2023-04-21](https://matrix.org/blog/2023/04/21/this-week-in-matrix-2023-04-21)

#### Stack
- Phoronix: [Mesa 23.0.3 Released With Another Batch Of Fixes](https://www.phoronix.com/news/Mesa-23.0.3-Released)

### Worth noting
* PINE64: [New community website! - help us make it better](https://www.reddit.com/r/PINE64official/comments/12p8lok/new_community_website_help_us_make_it_better/)
* joao.azevedo: [Gnome-screenshot adaptive (for some days now)](https://forums.puri.sm/t/gnome-screenshot-adaptive-for-some-days-now/20067)
* [Sebastian Krzyszkowiak: "chilling in the sun #shotonlibrem5 #nosliders" - Librem Social](https://social.librem.one/@dos/110248223663425035)
* [Robert Mader: "More hacking @ #LAS2023: imple…" - FLOSS.social](https://floss.social/@rmader/110243833969649613)
* [Robert Mader: "Hacking at #LAS2023: first vid…" - FLOSS.social](https://floss.social/@rmader/110243117365454172)
* [This Week in KDE: "This week in KDE: Stomping all…" - OpenDesktop Social](https://social.opendesktop.org/@thisweekinkde/110240307822238884)
* [caleb ×: "if you too want the smOOOth wi…" - Fosstodon](https://fosstodon.org/@calebccff/110240258621679517)
* [Bartłomiej Piotrowski: "A redesign of #Flathub is live…" - Treehouse Mastodon](https://social.treehouse.systems/@barthalion/110241570255988969)
* [Justine Smithies \~:idle: :tux:: "For all you #LinuxMobile and #…" - Fosstodon](https://fosstodon.org/@JustineSmithies/110226880951435172)
* [Joachim Breitner: "@JustineSmithies A tiling wind…" - Mastodon](https://mastodon.online/@nomeata/110227231378996616)
* [Danct12 on Twitter: "Booted @esxi\_arm on @thepine64 #PineTab2.](https://twitter.com/RealDanct12/status/1648798282179817472)

### Events
- LINux on MOBile: [See you at Linux-Infotag in Augsburg, Germany on April 29th, 2023!](https://linmob.net/see-you-in-augsburg-lit-2023/) _Come by if you can!_

### Worth reading
- AksDev: [How I report bugs](https://www.akselmo.dev/2023/04/16/How-I-Report-Bugs.html)
- mjg59: [PSA: upgrade your LUKS key derivation function](https://mjg59.dreamwidth.org/66429.html)
- Lantashi: [A Librem 5? - I'm Spoiled](gemini://lantashifiles.com/gemlog/entries/2023-04-10.gmi), ([access through Gemini Portal](https://portal.mozz.us/gemini/lantashifiles.com/gemlog/entries/2023-04-10.gmi))
- farooqkz: [On the state of KaiOS and BananaHackers community as of summer of 2023](https://blog.bananahackers.net/farooqkz/on-the-state-of-kaios-and-bananahackers-community-as-of-summer-of-2023)
- Phoronix: [Redesigned Flathub Site Launches For Flatpak Apps](https://www.phoronix.com/news/Redesigned-Flathub-Launches)
- Purism: [Introducing Flatpaks on PureOS](https://puri.sm/posts/introducing-flatpaks-on-pureos/)
- Purism: [Purism and SLNT Announce Partnership to Provide Security to Mobile Devices](https://puri.sm/posts/purism-and-slnt-announce-partnership-to-provide-security-to-mobile-devices/)

### Worth listening
- postmarketOS Podcast: [#30 INTERVIEW: Natanael Copa (of Alpine Linux Fame)](https://cast.postmarketos.org/episode/30-Interview-Natanael-Copa-Alpine-Linux/). _Great episode!_

### Worth watching
- mikehenson5: [PinePhone OR PinePhone Pro (3of5) - Phosh - Setup - Syncthing - Squeekboard - Online Accounts](https://www.youtube.com/watch?v=esOT5PhCOLg)
- mikehenson5: [PinePhone OR PinePhone Pro (4of5) - Phosh - Setup - Biktorgj - osmin - brave](https://www.youtube.com/watch?v=QgFtFzPhXJw)
- Michael Tunnell: [LXQt Desktop, New SUSE CEO, Flathub Beta, PineTab 2, KDE Connect & More Linux News!](https://www.youtube.com/watch?v=FWcHcuucqMk)
- Continuum Gaming: [CG E361: Sailfish OS with AD – Can you play Android Games on a Xperia 10 III (CandyCrush to PUBG)](https://www.youtube.com/watch?v=HOdIrvQuA1s)
- Niccolò Ve: [Another BIG KDE Apps Update!](https://tube.kockatoo.org/w/18dc0046-2e58-4c34-a982-4f9d3e198b76) 

#### Linux App Summit 2023
- Jonas Dreßler, Robert Mader, Tobias Bernard: [GNOME Mobile Show & Tell](https://www.youtube.com/live/watch?v=J7-3Qj_oVMM&t=14272), ([Schedule entry](https://conf.linuxappsummit.org/event/5/contributions/161/)) _Great session!_
- Volker Krause: [UnifiedPush - Push notifications for Linux](https://www.youtube.com/live/3-_aUNZMvko?feature=share&t=4976), ([Schedule entry](https://conf.linuxappsummit.org/event/5/contributions/149/))
- Jozef Mlich: [Nemomobile](https://www.youtube.com/live/J7-3Qj_oVMM?feature=share&t=24818), ([Schedule entry](https://conf.linuxappsummit.org/event/5/contributions/178/))
- Jorge Castro, Robert McQueen: [Flathub in 2023 - Revolutionizing the Linux App Ecosystem](https://www.youtube.com/live/jwMEDI4WsAE?feature=share&t=15513), ([Schedule entry](https://conf.linuxappsummit.org/event/5/contributions/173/))

### Thanks
Huge thanks to an anonymous contributor and to Plata for [the nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot.

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A?edit) for the next one! Since I am collecting many things there, this get's you early access if you will ;-) __If you just stumble on a thing, please put it in there too - all help is appreciated!__

